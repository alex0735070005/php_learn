var sendButton = document.querySelector('#sendData');

var inputName  = document.querySelector('#name');
var erName     = inputName.nextElementSibling;

var inputEmail = document.querySelector('#email');
var erEmail    = inputEmail.nextElementSibling;

var inputPassword = document.querySelector('#password');
var erPassword    = inputPassword.nextElementSibling;

sendButton.onclick = function()
{
    var data = {
        name:inputName.value,
        email:inputEmail.value,
        password:inputPassword.value
    };
    
    if(!valid(data)) return null;
    
    fetch('/register', {
        method:'POST',
        body:JSON.stringify(data)
    }).then(function(response){
        return response.json();
    }).then(function(resp){
        if(resp.answer === 'success')
        {
            alert('Registration success !');
            
            inputName.value = '';
            inputEmail.value = '';
            inputPassword.value = '';
        }else{
            alert(resp.answer);
        }
    });
};

function valid(data)
{
    var isValid = true;
    
    erEmail.classList.add('d-none');
    erName.classList.add('d-none');
    erPassword.classList.add('d-none');
    
    if(!/^[a-zA-Z\-_0-9\.]{3,25}$/.test(data.name)){
        inputName.nextElementSibling.classList.toggle('d-none');
        isValid = false;
    }
    
    if(!/^[a-zA-Z\-_0-9\.]{3,25}@[a-z]{1,10}\.[a-z\.]{1,30}$/.test(data.email)){
        inputEmail.nextElementSibling.classList.toggle('d-none');
        isValid = false;
    }
    
    if(!/^[a-zA-Z\-_0-9\.]{3,25}$/.test(data.password)){
        inputPassword.nextElementSibling.classList.toggle('d-none');
        isValid = false;
    }
    
    return isValid;
}