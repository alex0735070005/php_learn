<?php

function saveData () 
{
    // Set path db.txt
    $path = './data/db.txt';

    // Structure db
    $dataDb = [
        'users' => [],
        'images' => []
    ];

    // If isset file db.txt get json string and decode in array
    if(is_file($path))
    {
        $data = json_decode(file_get_contents($path), true);

    }else{
        // Save first structure db
        $data = $dataDb;
    }

    
    
    // Get quary params
    $dataPost = json_decode(file_get_contents('php://input'), true);
    
    if(!valid($dataPost)) {
        echo json_encode(['answer'=> 'not valid data']);
        return;
    }
    
    // Open file db.txt or create and open
    $resource = fopen($path, 'w');
    
    $email = $dataPost['email'];

    // Add new user to db array
    $data['users'][$email] = $dataPost;

    // Save data to db.txt
    fwrite($resource, json_encode($data));

    // Close resource db.txt
    fclose($resource);
    
    echo json_encode(['answer'=> 'success']);
}

function getUsers()
{
    // Set path db.txt
    $path = './data/db.txt';
    
    $data = json_decode(file_get_contents($path), true);
    
    return $data['users'];
}

function valid($data)
{
    if(!preg_match('/^[a-zA-Z\-_0-9\.]{3,25}$/', $data['name'])){
        return false;
    }
    
    if(!preg_match('/^[a-zA-Z\-_0-9\.]{3,25}@[a-z]{1,10}\.[a-z\.]{1,30}$/', $data['email'])){
        return false;
    }
    
    if(!preg_match('/^[a-zA-Z\-_0-9\.]{3,25}$/', $data['password'])){
        return false;
    }
    
    return true;
}