<?php

class Db {
    
    private $dbPath = './data/db.txt';
    
    private $schema = [
        'users'     => [],
        'images'    => []
    ];
    
    private $dataDb = [];
    
    
    public function __construct() 
    {
        // If isset file db.txt get json string and decode in array
        if(is_file($this->dbPath))
        {
            $this->dataDb = json_decode(file_get_contents($this->dbPath), true);

        }else{
            // Save first structure db
            $this->dataDb = $this->schema;
        }
    }

    public function saveData(string $name, array $data, $key)
    {
        // Open file db.txt or create and open
       $resource = fopen($this->dbPath, 'w');

       // Add new user to db array
       $this->dataDb[$name][$key] = $data;

       // Save data to db.txt
       fwrite($resource, json_encode($this->dataDb));

       // Close resource db.txt
       fclose($resource);
       
       return true;
    }
    
    public function getData($name)
    {
        $data = json_decode(file_get_contents($this->dbPath), true);

        return $data[$name];
    }
    
}


class User 
{
    public function valid($data)
    {
        if(!preg_match('/^[a-zA-Z\-_0-9\.]{3,25}$/', $data['name'])){
            return false;
        }

        if(!preg_match('/^[a-zA-Z\-_0-9\.]{3,25}@[a-z]{1,10}\.[a-z\.]{1,30}$/', $data['email'])){
            return false;
        }

        if(!preg_match('/^[a-zA-Z\-_0-9\.]{3,25}$/', $data['password'])){
            return false;
        }

        return true;
    }
}


