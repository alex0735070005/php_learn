<?php


spl_autoload_register(function ($namespace) {
    
    $path = str_replace('\\', '/', $namespace);
    
    include $path . '.php';
    
});
