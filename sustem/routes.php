<?php

if($_SERVER['REQUEST_METHOD']  !== 'POST'){
    require './pages/header.php';
}

if($root === '/'){
    require './pages/home.php';
}

if($root === 'category'){
    require './pages/category.php';
}

if($root === 'gallery'){
    require './pages/gallery.php';
}

if($root === 'registration'){
    require './pages/registration.php';
}

if($root === 'success-registation'){
    require './pages/successRegister.php';
}

/*if($root === 'register'){
    if(saveData()){
        header('location:/success-registation');
    }
}*/

if($root === 'register')
{
   /*Get data post json*/
   $data = json_decode(file_get_contents('php://input'), true);
   
   if($Db->saveData('users', $data, $data['email'])){
       echo json_encode(['answer'=>'success']);
   }
}

if($root === 'auth')
{
   /*Get data post json*/
   $data = json_decode(file_get_contents('php://input'), true);
   
   $users = $Db->getData('users');
   
   $email = $data['email'];
   $password = $data['password'];
   
   $user = [];
   
   if(isset($users[$email])){
       $user = $users[$email];
   }
   
   if($user['password'] === $password){
       $_SESSION['user'] = $user;
       echo json_encode(['answer'=>'success']);
   }else{
       echo json_encode(['answer'=>'user_is_not']);
   }
   die;
}

if($root === 'login'){
     require './pages/login.php';
}

if($root === 'users'){
     require './pages/users.php';
}

if($_SERVER['REQUEST_METHOD']  !== 'POST'){
    require './pages/footer.php';
}