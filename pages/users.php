<h1>Users</h1>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>name</th>
            <th>email</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach ($Db->getData('users') as $user) { $i++ ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $user['name'] ?></td>
                <td><?= $user['email'] ?></td>
            </tr>
        <?php } ?>
            
    </tbody>
</table>