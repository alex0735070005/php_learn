<div id="registration" class="d-flex justify-content-center">
    <form action="/register" method="POST">
        <div class="form-group">
              <label for="name">User name</label>
              <input name="name" type="text" class="form-control" id="name" >
              <small id="namelHelp" class="d-none form-text text-muted">
                  Not valid name (a-z A-Z min 3 chars max 25)
              </small>
        </div>
        <div class="form-group">
              <label for="email">Email address</label>
              <input name="email" type="email" class="form-control" id="email" >
              <small id="emailHelp" class="d-none form-text text-muted">
                  Not valid email (a-z A-Z @ a-z A-Z . a-z A-Z)
              </small>
        </div>
        <div class="form-group">
              <label for="password">Password</label>
              <input name = "password" type="password" class="form-control" id="password" >
              <small id="emailHelp" class="d-none form-text text-muted">
                  Not valid email (0-9 a-z A-Z min 6 max 25 chars)
              </small>
        </div>
        <button id="sendData" type="button" class="btn btn-primary">Submit</button>
    </form>
</div>
<script src="/public/registration.js"></script>