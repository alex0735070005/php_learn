<div id="registration" class="d-flex justify-content-center">
    <form>       
        <div class="form-group">
              <label for="email">Email address</label>
              <input name="email" type="email" class="form-control" id="email" >
              <small id="emailHelp" class="d-none form-text text-muted">
                  Not valid email (a-z A-Z @ a-z A-Z . a-z A-Z)
              </small>
        </div>
        <div class="form-group">
              <label for="password">Password</label>
              <input name = "password" type="password" class="form-control" id="password" >
              <small id="emailHelp" class="d-none form-text text-muted">
                  Not valid email (0-9 a-z A-Z min 6 max 25 chars)
              </small>
        </div>
        <button id="sendData" type="button" class="btn btn-primary">Submit</button>
    </form>
</div>
<script src="/public/login.js"></script>