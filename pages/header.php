<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="public/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="public/style.css" rel="stylesheet" />
    </head>
    <body class="container">
        <header class="mt-3">
            <nav class="nav">
                
                <?php foreach ($menu as $k => $name) { ?>
                    <li>
                        <a href="<?= $k ?>" class="<?= $root === $k ? 'active':'' ?>">
                            <?= $name ?>
                        </a>
                    </li>
                <?php } ?>    
                
            </nav>
        </header>
        <div class="page">